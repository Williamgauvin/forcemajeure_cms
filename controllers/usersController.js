const { UserModel } = require('../models/users');
const { ArticleModel } = require('../models/article');
const bcrypt = require('bcrypt');

// GET connection page CMS
exports.login = async (req, res, next) => {
    try {
        res.render('admin/login', { title: "Connexion au CMS" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation to login");
    }
}
exports.logout = async (req, res, next) => {
    try {
        req.session.destroy((err) => {
            res.render('admin/login', { title: "Connexion au CMS", erreur: "Vous avez bien été déconnecté de votre session!" });
        })

    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to logout");
    }
}
// Login CMS Admin
exports.authUser = async (req, res, next) => {
    try {
        const user = await UserModel.findOne({ username: req.body.username });
        if (user) {
            let compare = await bcrypt.compare(req.body.password, user.password);
            if (compare) {
                req.session.authenticated = true;
                let numArticle = await ArticleModel.count();
                let numUser = await UserModel.count();
                res.render('admin/', { title: "Accueil CMS", users: user, numArticle: numArticle, numUser: numUser })
            } else {
                res.render('admin/login', { title: "Connexion au CMS", erreur: "Mauvais nom d'utilisateur ou mot de passe" });
            }
        } else {
            res.render('admin/login', { title: "Connexion au CMS", erreur: "Mauvais nom d'utilisateur ou mot de passe" });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
// GET index CMS
exports.getIndexCMS = async (req, res, next) => {
    try {
        let numArticle = await ArticleModel.count()
        let numUser = await UserModel.count()
        var user = await UserModel.findOne({ username: req.body.username });
        res.render('admin/', { title: "Accueil CMS", users: user, numArticle: numArticle, numUser: numUser });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation article");
    }
}
exports.getAddPage = async (req, res, next) => {
    try {
        res.render('admin/add', { title: "Ajout d'article" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation article");
    }
}
exports.getAddUserPage = async (req, res, next) => {
    try {
        res.render('admin/adduser', { title: "Ajouter un admin" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation article");
    }
}
exports.getModerationArticlePage = async (req, res, next) => {
    try {
        let numArticle = await ArticleModel.count()
        let document = await ArticleModel.find({});
        res.render('admin/moderationarticle', { title: "Modération des articles", articles: document, numArticle: numArticle });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation article");
    }
}
// Moderation for articles
exports.getEditPage = async (req, res, next) => {
    try {
        let document = await getArticleById(req.params.id);
        res.render('admin/edit', {title: "Modifier l'article", articles: document })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.updateArticle = async (req, res, next) => {
    try {
        await ArticleModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/admin/moderationarticle');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to update article");
    }
}
exports.deleteArticle = async (req, res, next) => {
    try {
        await ArticleModel.findByIdAndRemove(req.params.id)
        res.redirect('/admin/moderationarticle');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to delete article");
    }
}



// Add admin
exports.addAdmin = async (req, res, next) => {
    try {
        const hashed_password = await bcrypt.hash(req.body.password, 12);
        const user = new UserModel({
            username: req.body.username,
            password: hashed_password
        });
        user.save((err, document) => {
            if (err) res.render('add', { error: err });
            res.render('admin/', { data: document });
        });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

// Add page function add
exports.addAticle = async (req, res, next) => {
    try {
        let data = new ArticleModel(req.body);
        data.save(function (err, data) {
            res.redirect('/admin/add')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_ARTICLE");
    }
}
exports.addService = async (req, res, next) => {
    try {
        let data = new ServiceModel(req.body);
        data.save(function (err, data) {
            res.redirect('/admin/add')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_ARTICLE");
    }
}
exports.addLicence = async (req, res, next) => {
    try {
        let data = new LicenceModel(req.body);
        data.save(function (err, data) {
            res.redirect('/admin/add')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.addPropo = async (req, res, next) => {
    try {
        let data = new PropoModel(req.body);
        data.save(function (err, data) {
            res.redirect('/admin/add')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.addTeamMate = async (req, res, next) => {
    try {
        let data = new TeamMateModel(req.body);
        data.save(function (err, data) {
            res.redirect('/admin/add')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}


const getArticleById = async (id) => {
    return await ArticleModel.findById(id);
}