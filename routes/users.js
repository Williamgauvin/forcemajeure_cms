var express = require('express');
var router = express.Router();
let usersController = require('../controllers/usersController')
var auth = require('../auth/auth')

// GET login admin page
router.get('/login', usersController.login);

// POST login admin
router.post('/login', usersController.authUser);

// GET page CMS
router.get('/', auth, usersController.getIndexCMS);
router.get('/add', auth, usersController.getAddPage);
router.get('/adduser', auth, usersController.getAddUserPage);
router.get('/moderationarticle', auth, usersController.getModerationArticlePage);
router.get('/edit/:id', auth, usersController.getEditPage);
router.get('/delete/:id', auth, usersController.deleteArticle);
router.get('/logout', auth, usersController.logout);

// POST element in DB
router.post('/add-article', auth, usersController.addAticle);
router.post('/add-service', auth, usersController.addService);
router.post('/add-licence', auth, usersController.addLicence);
router.post('/add-propo', auth, usersController.addPropo);
router.post('/add-teamMate', auth, usersController.addTeamMate);
router.post('/add-admin', auth, usersController.addAdmin);

// POST update article
router.post('/edit/:id', auth, usersController.updateArticle);

module.exports = router;