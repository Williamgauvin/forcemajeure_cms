var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let ArticleSchema = new Schema({
    title: String,
    category: String,
    author: String,
    resum: String,
    content: String,
    month: String,
    datePublished: String,
    img: String,
}, {
    timestamps: true
});
const ArticleModel = Mongoose.model("Articles", ArticleSchema);

module.exports = {
    model: ArticleModel,
    getAllArticle: async ()=> {
        return await ArticleModel.find({});
    },
    getArticleById: async (_id)=> {
        return await ArticleModel.findById(_id);
    },
    getAPIByActualites: async ()=> {
        let actualites = ArticleModel.find({category: "Actualites"});
        return await (actualites);
    },
    getAPIByConseils: async ()=> {
        let conseils = ArticleModel.find({category: "Conseils"});
        return await (conseils);
    },
    getAPIByReferencement: async ()=> {
        let referencement = ArticleModel.find({category: "Referencement"});
        return await (referencement);
    },
    getAPIByAnalytics: async ()=> {
        let analytics = ArticleModel.find({category: "Analytics"});
        return await (analytics);
    },
    getAPIByWeb: async ()=> {
        let web = ArticleModel.find({category: "Web"});
        return await (web);
    },
}
