var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let ContactSchema = new Schema({
    fullname: String,
    email: String,
    numTel: String,
    message: String,

}, {
    timestamps: true
});
exports.ContactModel = Mongoose.model("Contact", ContactSchema);